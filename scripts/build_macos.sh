#!/usr/bin/env bash
set -euo pipefail

# This script builds QEMU for macOS.

mkdir ../build
cd ../build

# the --disable-XYZ options below are copied from tests/docker/dockerfiles/qemu-system-arm-linux.docker
# The only exception is the removal of "--disable-cocoa".
# Cocoa is a core macOS framework, it cannot be disabled when building for macOS.
../qemu/configure \
  --target-list=arm-softmmu \
  --disable-alsa \
  --disable-bpf \
  --disable-brlapi \
  --disable-curl \
  --disable-curses \
  --disable-dsound \
  --disable-fuse \
  --disable-fuse-lseek \
  --disable-gcrypt \
  --disable-docs \
  --disable-guest-agent \
  --disable-sdl \
  --disable-gtk \
  --disable-vnc \
  --disable-pa \
  --disable-oss \
  --disable-spice \
  --disable-spice-protocol \
  --disable-usb-redir \
  --disable-virglrenderer \
  --disable-tools \
  --disable-xen \
  --disable-gnutls \
  --disable-nettle \
  --disable-auth-pam \
  --disable-kvm \
  --disable-snappy \
  --disable-cap-ng \
  --disable-libusb \
  --disable-vhost-net \
  --disable-vhost-crypto \
  --disable-vhost-kernel \
  --disable-vhost-user \
  --disable-live-block-migration \
  --disable-replication \
  --disable-parallels \
  --disable-crypto-afalg \
  --disable-opengl \
  --disable-libdaxctl \
  --disable-libiscsi \
  --disable-libpmem \
  --disable-libnfs \
  --disable-vde \
  --disable-smartcard \
  --disable-glusterfs \
  --disable-rbd \
  --disable-lzo \
  --disable-numa \
  --disable-libssh \
  --disable-linux-aio \
  --python=/opt/homebrew/Cellar/python@3.12/3.12.0/bin/python3.12 `# workaround for build failing on macOS`
ninja

cd -

mv ../build/qemu-system-arm .
