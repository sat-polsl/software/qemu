FROM registry.fedoraproject.org/fedora:38 AS builder

# Please keep this list sorted alphabetically
ENV PACKAGES \
    bash \
    bc \
    bison \
    bzip2 \
    ca-certificates \
    ccache \
    ctags \
    dbus-daemon \
    diffutils \
    findutils \
    flex \
    gcc \
    gcovr \
    git \
    glib2-devel \
    glibc-langpack-en \
    hostname \
    llvm \
    make \
    meson \
    mtools \
    ninja-build \
    nmap-ncat \
    openssh-clients \
    pcre-static \
    python3 \
    python3-PyYAML \
    python3-numpy \
    python3-opencv \
    python3-pillow \
    python3-pip \
    python3-sphinx \
    python3-sphinx_rtd_theme \
    sed \
    socat \
    sparse \
    spice-protocol \
    tar \
    tesseract \
    tesseract-langpack-eng \
    util-linux \
    which \
    xorriso \
    zstd \
    mingw32-nsis \
    mingw64-SDL2 \
    mingw64-SDL2_image \
    mingw64-bzip2 \
    mingw64-curl \
    mingw64-gcc \
    mingw64-gcc-c++ \
    mingw64-gettext \
    mingw64-glib2 \
    mingw64-gnutls \
    mingw64-gtk3 \
    mingw64-libepoxy \
    mingw64-libgcrypt \
    mingw64-libjpeg-turbo \
    mingw64-libpng \
    mingw64-libtasn1 \
    mingw64-nettle \
    mingw64-pixman \
    mingw64-pkg-config

RUN dnf install -y $PACKAGES
RUN rpm -q $PACKAGES | sort > /packages.txt
RUN mkdir -p /usr/libexec/ccache-wrappers && \
    ln -s /usr/bin/ccache /usr/libexec/ccache-wrappers/x86_64-w64-mingw32-c++ && \
    ln -s /usr/bin/ccache /usr/libexec/ccache-wrappers/x86_64-w64-mingw32-cc && \
    ln -s /usr/bin/ccache /usr/libexec/ccache-wrappers/x86_64-w64-mingw32-g++ && \
    ln -s /usr/bin/ccache /usr/libexec/ccache-wrappers/x86_64-w64-mingw32-gcc

COPY . /qemu
WORKDIR /qemu

RUN mkdir build \
    && cd build \
    && ../configure \
           --target-list=arm-softmmu \
           --cross-prefix=x86_64-w64-mingw32- \
           --disable-alsa \
           --disable-bpf \
           --disable-brlapi \
           --disable-cocoa \
           --disable-curl \
           --disable-curses \
           --disable-dsound \
           --disable-fuse \
           --disable-fuse-lseek \
           --disable-gcrypt \
           --disable-docs \
           --disable-guest-agent \
           --disable-sdl \
           --disable-gtk \
           --disable-vnc \
           --disable-pa \
           --disable-oss \
           --disable-spice \
           --disable-spice-protocol \
           --disable-usb-redir \
           --disable-virglrenderer \
           --disable-tools \
           --disable-xen \
           --disable-gnutls \
           --disable-nettle \
           --disable-auth-pam \
           --disable-kvm \
           --disable-snappy \
           --disable-cap-ng \
           --disable-libusb \
           --disable-vhost-net \
           --disable-vhost-crypto \
           --disable-vhost-kernel \
           --disable-vhost-user \
           --disable-live-block-migration \
           --disable-replication \
           --disable-parallels \
           --disable-crypto-afalg \
           --disable-opengl \
           --disable-libdaxctl \
           --disable-libiscsi \
           --disable-libpmem \
           --disable-libnfs \
           --disable-vde \
           --disable-smartcard \
           --disable-glusterfs \
           --disable-rbd \
           --disable-lzo \
           --disable-numa \
           --disable-libssh \
           --disable-linux-aio \
    && ninja

RUN ls -la /usr/x86_64-w64-mingw32/sys-root/mingw/bin/

RUN mkdir archive \
    && cd archive \
    && cp /qemu/build/qemu-system-arm.exe . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libglib-2.0-0.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libgio-2.0-0.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpixman-1-0.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libbz2-1.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libwinpthread-1.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libgcc_s_seh-1.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libstdc++-6.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/zlib1.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libgobject-2.0-0.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libssp-0.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libffi-8.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libintl-8.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpng16-16.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpcre2-8-0.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libgmodule-2.0-0.dll . \
    && cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/iconv.dll . \
    && zip /qemu/build/qemu-system-arm-w64.zip *

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /qemu/build/qemu-system-arm-w64.zip ./

